USE CompanyDB;
GO
DROP TABLE IF EXISTS [dbo].Client;
GO
CREATE TABLE [dbo].Client (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    [Address] NVARCHAR(MAX)
    CONSTRAINT DF_Client_Address DEFAULT 0,
    [Phone] VARCHAR(22)
    CONSTRAINT DF_Client_Phone DEFAULT 0,
    [Sex] VARCHAR(1) NOT NULL,
    CONSTRAINT PK_Client_ID PRIMARY KEY (ID),
);
INSERT INTO dbo.[Client]([Name], [Phone], [Sex]) VALUES
    ('Don Dodd', '+86-135-5539-6856', 'M'),  
    ('Daniela Rigby', '+86-185-5571-1835', 'W'),    
    ('Devante Shepherd', '+86-135-5560-8673', 'M'),    
    ('Abbi Lindsey', '+86-155-5542-1203', 'W'),    
    ('Reagan Lyons', '+86-185-5551-7240', 'W'),    
    ('Ethel Clay', '+1-202-555-0128', 'M'),       
    ('Talhah Combs', '', 'W');