USE CompanyDB;
GO
DROP TABLE IF EXISTS [dbo].[Provider];
GO
CREATE TABLE [dbo].[Provider] (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    CONSTRAINT PK_Provider_ID PRIMARY KEY (ID),
);
GO
INSERT INTO dbo.[Provider]([Name]) VALUES
    ('Tradeprism'),
    ('Automation Tea'),
    ('Recovery Elephant'),
    ('Attune City'),
    ('99 problems');