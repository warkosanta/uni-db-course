USE CompanyDB;
GO
DROP TABLE IF EXISTS [dbo].Employee;
GO
CREATE TABLE [dbo].Employee (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    [ID_Manager] INT,
    CONSTRAINT PK_Employee_ID PRIMARY KEY (ID),
    CONSTRAINT FK_Employee_To_Employee FOREIGN KEY (ID_Manager) REFERENCES Employee (ID) ON DELETE NO ACTION
);
INSERT INTO dbo.Employee([Name], ID_Manager) VALUES
   ('Hilary Emerson', null),
   ('Caiden Murphy', 1),
   ('Crystal Parsons', 1),
   ('Darci Lennon', 2),
   ('Anisa Hoffman', 2),
   ('Rudra Oconnor', 2),
   ('Kitty Irwin', 3),
   ('Devan Barrow', 3),
   ('Simran Sweet', 3);
