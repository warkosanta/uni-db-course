USE CompanyDB;
GO
DROP TABLE IF EXISTS [dbo].Store;
GO
CREATE TABLE [dbo].Store (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [ID_Provider] INT NOT NULL,
    [ID_Product] INT NOT NULL,
    [Quantity] NVARCHAR(255),
    [Price] DECIMAL NOT NULL
    CONSTRAINT DF_Store_Price DEFAULT 0,
    [Date] DATE NOT NULL
    CONSTRAINT DF_Store_Date DEFAULT GETDATE(),
    CONSTRAINT PK_Store_ID PRIMARY KEY (ID),
    CONSTRAINT FK_Store_To_Provider FOREIGN KEY (ID_Provider) REFERENCES [Provider] (ID) ON DELETE CASCADE,
    CONSTRAINT FK_Store_To_Product FOREIGN KEY (ID_Product) REFERENCES [Product] (ID) ON DELETE CASCADE
);
INSERT INTO dbo.Store(ID_Provider, ID_Product, Price, [Date], Quantity) VALUES   
    (1,1, 100, '2020-02-26', 10),
    (2,1, 110, '2020-02-21', 15),
    (2,4, 50, '2020-02-27', 1),
    (2,4, 200, '2020-02-28', 1),
    (3,6, 350, '2020-02-28', 0),
    (3,7, 450, '2020-02-28', 0),
    (3,8, 100, '2020-02-28', 1),
    (4,9, 20, '2020-02-28', 0);