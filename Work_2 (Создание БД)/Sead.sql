USE CompanyDB;
GO
INSERT INTO dbo.[Provider]([Name]) VALUES
    ('Tradeprism'),
    ('Automation Tea'),
    ('Recovery Elephant'),
    ('Attune City'),
    ('99 problems');
GO
INSERT INTO dbo.[Product]([Name], [Unit], [Category]) VALUES
    ('EKOLN - Trash can','1','Bathroom accessories'),
    ('BROGRUND - 3-piece bathroom set','3','Bathroom accessories'),
    ('BORSTAD - Cedar block','9','Home fragrance'),
    ('NJUTNING - Vase and 6 scented sticks','6','Home fragrance'),
    ('KALLRÖR','2','Home improvement'),
    ('ÖSTERNÄS','2','Home improvement'),
    ('ODGER Swivel chair','1','Chairs'),
    ('ANTILOP Support pillow','1','Chairs'),
    ('PINNARP','98x1 1/2','Kitchen countertops'),
    ('STOCKARYD','0.528 qt','Kitchen countertops');
GO
INSERT INTO dbo.[Client]([Name], [Phone], [Sex]) VALUES
    ('Don Dodd', '+86-135-5539-6856', 'M'),  
    ('Daniela Rigby', '+86-185-5571-1835', 'W'),    
    ('Devante Shepherd', '+86-135-5560-8673', 'M'),    
    ('Abbi Lindsey', '+86-155-5542-1203', 'W'),    
    ('Reagan Lyons', '+86-185-5551-7240', 'W'),    
    ('Ethel Clay', '+1-202-555-0128', 'M'),       
    ('Talhah Combs', '', 'W');
GO
INSERT INTO dbo.Employee([Name], ID_Manager) VALUES
   ('Hilary Emerson', null),
   ('Caiden Murphy', 1),
   ('Crystal Parsons', 1),
   ('Darci Lennon', 2),
   ('Anisa Hoffman', 2),
   ('Rudra Oconnor', 2),
   ('Kitty Irwin', 3),
   ('Devan Barrow', 3),
   ('Simran Sweet', 3);
GO
INSERT INTO dbo.Store(ID_Provider, ID_Product, Price, [Date], Quantity) VALUES   
    (1,1, 100, '2020-02-26', 10),
    (2,1, 110, '2020-02-21', 15),
    (2,4, 50, '2020-02-27', 1),
    (2,4, 200, '2020-02-28', 1),
    (3,6, 350, '2020-02-28', 0),
    (3,7, 450, '2020-02-28', 0),
    (3,8, 100, '2020-02-28', 1),
    (4,9, 20, '2020-02-28', 0);
GO 
INSERT INTO dbo.[Deal](ID_Store, ID_Client, ID_Employee, Quantity, Discount) VALUES
    (1,1,4,100,0),
    (1,2,7,200,5),
    (2,3,5,50,0),
    (2,4,3,10,0),
    (3,5,3,600,10),
    (4,6,6,5,0);
