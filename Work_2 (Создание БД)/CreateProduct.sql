USE CompanyDB;
GO
DROP TABLE IF EXISTS [dbo].Product;
GO
CREATE TABLE [dbo].Product (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Category] NVARCHAR(255) NOT NULL,
    [Unit] NVARCHAR(10) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    [Description] TEXT,
    CONSTRAINT PK_Product_ID PRIMARY KEY (ID),
);
INSERT INTO dbo.[Product]([Name], [Unit], [Category]) VALUES
    ('EKOLN - Trash can','1','Bathroom accessories'),
    ('BROGRUND - 3-piece bathroom set','3','Bathroom accessories'),
    ('BORSTAD - Cedar block','9','Home fragrance'),
    ('NJUTNING - Vase and 6 scented sticks','6','Home fragrance'),
    ('KALLRÖR','2','Home improvement'),
    ('ÖSTERNÄS','2','Home improvement'),
    ('ODGER Swivel chair','1','Chairs'),
    ('ANTILOP Support pillow','1','Chairs'),
    ('PINNARP','98x1 1/2','Kitchen countertops'),
    ('STOCKARYD','0.528 qt','Kitchen countertops');
