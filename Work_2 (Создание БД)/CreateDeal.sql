USE CompanyDB;
GO
DROP TABLE IF EXISTS [dbo].Deal;
GO
CREATE TABLE [dbo].Deal (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [ID_Store] INT NOT NULL,
    [ID_Client] INT NOT NULL,
    [ID_Employee] INT NOT NULL,
    [Quantity] NVARCHAR(255) NOT NULL,
    [Discount] DECIMAL(5,2) NOT NULL,
    [Date] DATE NOT NULL
    CONSTRAINT DK_Deal_Date DEFAULT GETDATE(),
    CONSTRAINT PK_Deal_ID PRIMARY KEY (ID),
    CONSTRAINT FK_Deal_To_Store FOREIGN KEY (ID_Store) REFERENCES Store (ID) ON DELETE CASCADE,
    CONSTRAINT FK_Deal_To_Client FOREIGN KEY (ID_Client) REFERENCES Client (ID) ON DELETE CASCADE,
    CONSTRAINT FK_Deal_To_Employee FOREIGN KEY (ID_Employee) REFERENCES Employee (ID) ON DELETE CASCADE,
);
INSERT INTO dbo.[Deal](ID_Store, ID_Client, ID_Employee, Quantity, Discount) VALUES
    (1,1,4,100,0),
    (1,2,7,200,5),
    (2,3,5,50,0),
    (2,4,3,10,0),
    (3,5,3,600,10),
    (4,6,6,5,0);