USE [DeanDB]
--1. Создайте группу студентов, обучающихся по специальности, на которую студентов не набирали.
DECLARE @newGroupName NVARCHAR(50) = 'КИ19-14Б' -- Имя новой группы
DECLARE @oldGroupID NVARCHAR(50)-- ID группы ИЗ которой будем переводить студентов
DECLARE @newGroupID NVARCHAR(50)-- ID группы В которую будем переводить студентов

-- Если группы еще несуществует, создаем ее с пустующей специальностью
IF @newGroupName IN (SELECT NAME FROM [Group])
		PRINT 'Group with name ' + @newGroupName + ' already exists.'
ELSE 
INSERT INTO [Group](Name, Speciality_ID) 
VALUES (@newGroupName, (SELECT ID
						FROM Speciality
						WHERE ID NOT IN (SELECT DISTINCT Speciality_ID FROM [Group])))

SET @newGroupID = (SELECT ID FROM [Group] WHERE [Name] = @newGroupName)

--2. Одну из групп, которых две по одной специальности (см. требования Лаб. раб. №7),
--перевести в созданную группу (на другую специальность).
SET @oldGroupID = (
					SELECT TOP 1 [Group].ID
					FROM [Group]
					WHERE Speciality_ID =  (select TOP 1 Speciality.ID
											from [Group] INNER JOIN [Speciality] ON [Group].Speciality_ID = Speciality.ID 
											GROUP BY Speciality.Name, Speciality.ID
											HAVING COUNT([Group].Name) >= 2)
					ORDER BY NEWID())

-- Обновляем ID у группы
UPDATE Student
SET Group_ID = (SELECT ID FROM [Group] WHERE [Name] = @newGroupName)
WHERE Group_ID = @oldGroupID

--3. Одну из групп, которых две по одной специальности (см. требования Лаб. раб. №7),
--перевести в созданную группу (на другую специальность).

--Куратор
UPDATE [Group]
SET Mentor_ID = (SELECT Mentor_ID FROM [Group] WHERE ID = @oldGroupID)
WHERE [Group].ID = @newGroupID

-- Староста
UPDATE [Group]
SET Praepostor_ID = (SELECT Praepostor_ID FROM [Group] WHERE ID = @oldGroupID)
WHERE [Group].ID = @newGroupID

--4. Удалите пустую группу из таблицы.
DELETE FROM [Group]
WHERE ID = @oldGroupID