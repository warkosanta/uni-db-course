/*Показать в представлении студентов, сдававших сессии на «Отлично». Вывести
Ф.И.О., номер группы и семестр. Группировать по семестрам и группам.*/
GO
CREATE VIEW StudentsMarks AS
SELECT Student.Firstname, Student.Lastname, Student.Patronymic,
[Group].Name, Subject.Term AS [Семестр]
FROM Student INNER JOIN [Group] ON Student.Group_ID = [Group].ID
INNER JOIN Study ON Student.ID = Study.Student_ID INNER JOIN Subject ON Study.Subject_ID = Subject.ID
WHERE Study.Result_Mark IN ('5','Зачтено')
GROUP BY Student.Firstname, Student.Lastname, Student.Patronymic, [Group].Name, Subject.Term
GO
SELECT * FROM StudentsMarks
--Напиши представление, которое будет содержать всю информацию о дисциплинах и количестве студентов их слушающих 
GO
DROP VIEW IF EXISTS GetDisciplines
GO
CREATE VIEW GetDisciplines AS
SELECT [Subject].Name AS [Предмет], Speciality.Name AS [Специальность], COUNT(Student.ID) AS [Слушатели]
FROM Subject INNER JOIN Speciality ON Subject.Speciality_ID = Speciality.ID
INNER JOIN [Group] ON Speciality.ID = [Group].Speciality_ID INNER JOIN Student ON Student.Group_ID = [Group].ID
GROUP BY [Subject].Name, Speciality.Name
GO
SELECT * FROM GetDisciplines