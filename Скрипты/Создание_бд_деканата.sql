USE [master]
GO
DROP DATABASE IF EXISTS [DeanDB]
CREATE DATABASE [DeanDB]
USE [DeanDB]
GO
-- �������� ���� ������
DROP TABLE IF EXISTS [Professor]
DROP TABLE IF EXISTS [Subject]
DROP TABLE IF EXISTS [Study]
DROP TABLE IF EXISTS [Subject_Professor]
DROP TABLE IF EXISTS [Specialities]
DROP TABLE IF EXISTS [Groups]
DROP TABLE IF EXISTS [Student]

-- �������� ������
CREATE TABLE [dbo].[Speciality] (
	[ID] INT IDENTITY(1,1) NOT NULL,
	[Code] NVARCHAR(50) NOT NULL,
	[Name] NVARCHAR(20) NOT NULL,
	[Education_Form] NVARCHAR(20) NOT NULL, 
	[Degree] NVARCHAR(20) NOT NULL,
	[Duration] INT NOT NULL,
	[Description] NVARCHAR(MAX)
)
GO
CREATE TABLE [dbo].[Group](
	[ID] INT IDENTITY(1,1) NOT NULL,
	[Name] NVARCHAR(256),
	[Speciality_ID] INT,
	[Praepostor_ID] INT,
	[Mentor_ID] INT
)
GO
CREATE TABLE [dbo].[Student] (
	[ID] INT NOT NULL,
	[Firstname] NVARCHAR(256) NOT NULL,
	[Lastname] NVARCHAR(256) NOT NULL,
	[Patronymic] NVARCHAR(256),
	[Group_ID] INT NOT NULL,
	[Enrollment_Year] VARCHAR(4)
)
GO
CREATE TABLE [dbo].[Professor] (
	[ID] INT NOT NULL,
	[Firstname] NVARCHAR(256) NOT NULL,
	[Lastname] NVARCHAR(256) NOT NULL,
	[Patronymic] NVARCHAR(256),
	[Degree] NVARCHAR(50), 
	[Rank] NVARCHAR(50) NOT NULL,
	[Department] NVARCHAR(256),
)
GO
CREATE TABLE [dbo].[Subject] (
	[ID] INT NOT NULL IDENTITY(1,1),
	[Name] NVARCHAR(256),
	[Speciality_ID] INT,
	[Term] INT,
	[Duration] INT,
	[Checkout_Form] NVARCHAR(256)
)
GO
CREATE TABLE [dbo].[Study] (
	[Student_ID] INT NOT NULL,
	[Subject_ID] INT NOT NULL,
	[Date] DATE,
	[Result_Mark] NVARCHAR(50),
)
CREATE TABLE [dbo].[Subject_Professor] (
	[Professor_ID] INT NOT NULL,
	[Subject_ID] INT NOT NULL,
)

-- ����������� ��� ������� "Speciality"
ALTER TABLE [dbo].[Speciality]
	ADD CONSTRAINT PK_Speciality_ID PRIMARY KEY (ID)

ALTER TABLE [dbo].[Speciality]
	ADD CONSTRAINT CHK_Speciality_Degree CHECK ([Degree] IN ('�����������', '�����������', '������������', '�����������'))

ALTER TABLE [dbo].[Speciality]
	ADD CONSTRAINT CHK_Speciality_EducationForm CHECK ([Education_Form] IN ('����', '������', '����-������'))

-- ����������� ��� ������� "Professor"
ALTER TABLE [dbo].[Professor]
	ADD CONSTRAINT PK_Professor_ID PRIMARY KEY (ID)

ALTER TABLE [dbo].[Professor]
	ADD CONSTRAINT CHK_Professor_Degree CHECK ([Degree] IN ('�������� ����', '������ ����'))
	
ALTER TABLE [dbo].[Professor]
	ADD CONSTRAINT CHK_Professor_Rank CHECK ([Rank] IN ('������', '���������'))

-- ����������� ��� ������� "Student"
ALTER TABLE [dbo].[Student]
	ADD CONSTRAINT PK_Student_ID PRIMARY KEY (ID)
	
ALTER TABLE [dbo].[Student]
	ADD CONSTRAINT CHK_Student_EnrollmentYear CHECK ([Enrollment_Year] BETWEEN 1000 AND 9999)

-- ����������� ��� ������� "Group"
ALTER TABLE [dbo].[Group]
	ADD CONSTRAINT PK_Group_ID PRIMARY KEY (ID)

ALTER TABLE [dbo].[Group]
	ADD CONSTRAINT FK_Group_MentorID FOREIGN KEY (Mentor_ID) 
	REFERENCES [dbo].[Professor](ID) ON UPDATE CASCADE

ALTER TABLE [dbo].[Group]
	ADD CONSTRAINT FK_Group_SpecialityID FOREIGN KEY (Speciality_ID) 
	REFERENCES [dbo].[Speciality](ID) ON DELETE NO ACTION

ALTER TABLE [dbo].[Student]
	ADD CONSTRAINT FK_Student_GroupID FOREIGN KEY (Group_ID) 
	REFERENCES [dbo].[Group](ID) ON DELETE NO ACTION

-- ����������� ��� ������� "Subject"
ALTER TABLE [dbo].[Subject]
	ADD CONSTRAINT PK_Subject_ID PRIMARY KEY (ID)

ALTER TABLE [dbo].[Subject]
	ADD CONSTRAINT CHK_Subject_CheckoutForm CHECK ([Checkout_Form] IN ('�������', '�����', '�������� ������', '����������� ������'))

-- ����������� ��� ������� "Study"
ALTER TABLE [dbo].[Study]
	ADD CONSTRAINT PK_Study_ID PRIMARY KEY ([Student_ID],[Subject_ID])

ALTER TABLE [dbo].[Study]
	ADD CONSTRAINT CHK_Study_Mark CHECK([Result_Mark] IN ('5', '4', '3', '�������', '������', '�����������������'))

ALTER TABLE [dbo].[Study]
	ADD CONSTRAINT FK_Study_StudentID FOREIGN KEY (Student_ID) 
	REFERENCES [dbo].[Student](ID) ON UPDATE CASCADE

ALTER TABLE [dbo].[Study]
	ADD CONSTRAINT FK_Study_SubjectID FOREIGN KEY (Subject_ID) 
	REFERENCES [dbo].[Subject](ID) ON UPDATE CASCADE
	
-- ����������� ��� ������� "Subject_Professor"
ALTER TABLE [dbo].[Subject_Professor]
	ADD CONSTRAINT PK_Subject_Professor_ID PRIMARY KEY ([Professor_ID],[Subject_ID])

ALTER TABLE [dbo].[Subject_Professor]
	ADD CONSTRAINT FK_Subject_Professor_ProfessorID FOREIGN KEY (Professor_ID) 
	REFERENCES [dbo].[Professor](ID) ON UPDATE CASCADE

ALTER TABLE [dbo].[Subject_Professor]
	ADD CONSTRAINT FK_Subject_Professor_SubjectID FOREIGN KEY (Subject_ID) 
	REFERENCES [dbo].[Subject](ID) ON UPDATE CASCADE

-- ���������� �������
INSERT INTO [dbo].[Speciality]([Code], [Name], [Education_Form], [Degree], [Duration], [Description])
VALUES 
('090302', '������', '����', '�����������', 4, '�������� ������������ ������'),
('090303', '������', '����', '������������', 2, '����������� �������� ������������ ������'),
('090301', '����������', '������', '�����������', 4, '������������� ����������')

INSERT INTO  [dbo].[Professor]
VALUES 
(1,'���������','������','���������', '�������� ����', '������', '������� �������'),
(2,'������', '�����', '���������', '�������� ����', '������', '������� ������ � ������������'),
(3,'����', '������', '����������', '������ ����', '���������', '������� ������ � ������������'), 
(4,'�', '����', '����������', '������ ����', '���������', '������� ������������ �����')

INSERT INTO [dbo].[Group]
VALUES 
('��18-13�', 1, 1, 1),
('��27-1', 1, 8, 2),
('��17-14�', 2, 15, 3),
('��18-18�', 2, 22, 4)

INSERT INTO  [dbo].[Student]
VALUES 

(1,'�������', '�������', '�������������',1,2000),
(2,'������', 'Ը���', '����������',1,2000),
(3,'��������', '����', '����������',1,2000),
(4,'�������', '������', '����������',1,2000),
(5,'������', '�����', '����������',1,2000),
(6,'������', '������', '����������',1,2000),
(7,'�����', '���������', '���������',1,2000),

(8,'��������', '�����', '����������',2,2000),
(9,'����� ', '�����', '����������',2,2000),
(10,'������', '�����', '�������������',2,2000),
(11,'�������', '��������', '��������',2,2000),
(12,'���������', '�������', '���������',2,2000),
(13,'������', '����', '����������',2,2000),
(14,'�����', '�����', '����������',2,2000),

(15,'����������', '�����', 'Ը�������',3,2000),
(16,'�����', '����', '����������',3,2000),
(17,'���������', '�������', '��������',3,2000),
(18,'������', '�����', '�����������',3,2000),
(19,'�������', '�������', '���������',3,2000),
(20,'���������', '�����', '���������',3,2000),
(21,'����', '������', '��������', 3, 1995),

(22,'����', '��������', '��������', 4, 1996),
(23,'������', '������', '����������', 4, 1995),
(24,'������', '��������', '����������', 4, 1996),
(25,'����', '�������', '�����', 4, 1996),
(26,'�������', '��������', '���������', 4, 1995),
(27,'�����', '��������', '���������', 4, 1996),
(28,'������', '�����', '����������', 4, 1996)

INSERT INTO [dbo].[Subject]
VALUES
('���������', 7, 1, 40, '�������'),
('���������', 8, 1, 40, '�������'),
('������', 1, 1, 70, '�����'),
('������', 1, 2, 70, '�������'),
('�����', 5, 1, 68, '�����'),
('�����', 5, 2, 68, '�������'),
('�������������� ������', 1, 1, 88, '�������'),
('�����������', 1, 1, 40, '�����'),
('����������������', 1, 2, 80, '�����'),
('�������', 1, 2, 88, '�����')

INSERT INTO [dbo].[Study]
VALUES

(1, 5,  '20200421', 5),
(2, 5,  '20200421', 3),
(3, 5,  '20200421', 4),
(4, 5,  '20200421', 5),
(5, 5,  '20200421', 4),
(6, 5,  '20200421', 4),
(7, 5,  '20200421', 3),
		 	  
(8, 3,  '20200527', 5),
(9, 3,  '20200527', 3),
(10, 3, '20200527', 4),
(11, 3, '20200527', 5),
(12, 3, '20200527', 4),
(13, 3, '20200527', 4),
(14, 3, '20200527', 3),

(8, 4,  '20200531', 5),
(9, 4,  '20200531', 3),
(10, 4, '20200531', 4),
(11, 4, '20200531', 5),
(12, 4, '20200531', 4),
(13, 4, '20200531', 4),
(14, 4, '20200531', 3),
		 	   
(15, 8, '20200531', 5),
(16, 8, '20200531', 3),
(17, 8, '20200531', 4),
(18, 8, '20200531', 5),
(19, 8, '20200531', 4),
(20, 8, '20200531', 4),
(21, 8, '20200531', 3),
	
(22, 7, '20200531', 5),
(23, 7, '20200531', 3),
(24, 7, '20200531', 4),
(25, 7, '20200531', 5),
(26, 7, '20200531', 4),
(27, 7, '20200531', 4),
(28, 7, '20200531', 3)

INSERT INTO [dbo].Subject_Professor
VALUES 
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 3),
(3, 4),
(3, 7),
(4, 5),
(4, 6)