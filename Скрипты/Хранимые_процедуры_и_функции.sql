/*1. Создать функцию, определяющую средний бал по указанной дисциплине.
Принимая идентификатор дисциплины, как входной параметр (например,
ID_Disciplin), функция возвращает вещественное значение среднего балла среди
всех студентов, сдававших экзамен.*/
CREATE FUNCTION GetAverage(@ID_Disciplin INT)
RETURNS REAL 
BEGIN
	DECLARE @AVGMARK REAL
	SELECT @AVGMARK = AVG(CONVERT(INT,Study.Result_Mark))
	FROM Study INNER JOIN Student ON Study.Student_ID = Student.ID
	INNER JOIN [Group] ON Student.Group_ID = [Group].ID
	WHERE [Group].Speciality = @ID_Disciplin AND CONVERT(INT, Study.Result_Mark) > 2
	RETURN @AVGMARK
END 
/*2. Создать процедуру, формирующую список зачетов и экзаменов, которые
необходимо сдать студентам указанной группы в указанный семестр (входные
параметры – группа и семестр).*/
GO
CREATE PROC GetCredits @GROUP REAL, @TERM REAL
AS 
BEGIN
	SELECT DISTINCT Subject.Name, Subject.Term, Subject.Checkout_Form
	FROM Subject INNER JOIN Study ON Study.Subject_ID = Subject.ID
		INNER JOIN Student ON Study.Student_ID = Student.ID
	WHERE (Subject.Checkout_Form = 'Зачет' OR Subject.Checkout_Form = 'Экзамен' ) 
		AND Subject.Term = @TERM 
		AND Student.Group_ID = @GROUP
END
-- Доп.задание
GO
CREATE FUNCTION GetMarks()
RETURNS TABLE
AS
RETURN (SELECT Student.Firstname, Student.Lastname, COUNT(Study.Result_Mark) AS [Кол-во оценок "4"]
		FROM Study INNER JOIN Student ON Study.Student_ID = Student.ID
		WHERE Study.Result_Mark = 4
		GROUP BY Student.Firstname, Student.Lastname)