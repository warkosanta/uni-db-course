USE CompanyDB
GO
--1. Сортировать поставщиков в алфавитном (обратном) порядке.
SELECT [Name] FROM [Provider]
ORDER BY [Name] DESC
GO
--2. Вывести поставщика(ов) с самым длинным названием.
SELECT TOP 1 [Name] AS [Longest provider name]
FROM [Provider]
ORDER BY LEN([Name]) DESC
GO
--3. Вывести всех покупателей с фамилией, начинающейся на букву «D».
SELECT [Name]
FROM Client
WHERE [Name] LIKE 'D%'
GO
/*4. Вывести всех покупателей с фамилией, начинающейся с букв в диапазоне от "В" до
"К", где вторая буква «о».*/
SELECT [Name]
FROM Client
WHERE [Name] LIKE '[B-K]o%'
GO
--5. Определите количество сделок за текущий календарный месяц.
SELECT COUNT(*) AS [Current month deals count]
FROM Deal
WHERE DATEPART(MONTH, [Date]) = DATEPART(MONTH, GETDATE()) AND
    DATEPART(YEAR, [Date]) = DATEPART(YEAR, GETDATE())
GO
/*6. Определите количество сделок, зафиксированных в заданные дни недели
(например, по вторникам и средам).*/
DECLARE @given_week_days TABLE([NUMBER] INT, [DAYOFWEEK] NVARCHAR(10))
INSERT INTO @given_week_days VALUES 
    (2, 'Вторник'), 
    (3, 'Среда'),
	(4, 'Четверг')

SELECT COUNT(*) AS [Given week days deals count]
    FROM CompanyDB.dbo.Deal
    WHERE DATEPART(WEEKDAY, [Date]) IN (
        SELECT NUMBER
        FROM @given_week_days)
