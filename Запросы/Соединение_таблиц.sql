USE CompanyDB
--1. Показать какой поставщик поставил каждый товар на склад (INNER JOIN).
GO
SELECT Product.Name AS [Product name], [Provider].Name AS [Provider name]
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product 
             INNER JOIN [Provider] ON [Provider].[ID] = Store.ID_Provider

--2. Вывести список товаров, которыми торгует фирма, и их поставщиков вне
--зависимости от наличия поставок (LEFT JOIN).
GO
SELECT Product.Name AS [Product name], [Provider].Name AS [Provider name], Store.Quantity
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product 
             LEFT JOIN [Provider] ON [Provider].[ID] = Store.ID_Provider

--3. Вывести информацию о покупаемых со склада товарах и их покупателях, включая
--товары, отсутствующие в списке реализованных товаров (RIGHT JOIN).
GO
SELECT Product.Name AS [Product name], 
CASE 
      WHEN Client.[Name] IS NULL THEN 'Не определено'
      ELSE Client.[Name]
END AS [Client name],
CASE 
      WHEN Deal.Quantity IS NULL THEN 'Не определено'
      ELSE Deal.Quantity
END AS [Deal quantity]
FROM Store INNER JOIN Deal ON Deal.ID_Store = Store.ID
           INNER JOIN Client ON [Client].ID = Deal.ID_Client
           RIGHT OUTER JOIN Product ON Store.ID_Product = Product.ID

/*4. Вывести список поставщиков, которые хотя бы раз осуществляли поставку на склад
(полусоединение).*/
GO
SELECT [Provider].Name AS [Provider name]
FROM [Provider] 
WHERE EXISTS (
      SELECT 1 
      FROM Store
      WHERE Store.ID_Provider = [Provider].ID
)
/*5. Выведите список сотрудников с указанием их прямых начальников
(самосоединение). Для главного начальника в столбец «Начальник» вывести
«не определен».
*/
GO
SELECT Employee_Manager.Name AS [Employee name], 
CASE 
      WHEN Employee.Name IS NULL THEN 'Не определен'
      ELSE Employee.Name
END [Manager name]
FROM Employee RIGHT OUTER JOIN Employee AS Employee_Manager ON Employee.ID = Employee_Manager.ID_Manager
--запрос, который вернет имя работника и товары, которые он продавал покупателю с именем "Сергей"
SELECT Employee.Name, Product.Name, Client.Name
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product
            INNER JOIN Deal ON Deal.ID_Store = Store.ID
            INNER JOIN  Client ON Deal.ID_Client = Client.ID
            INNER JOIN Employee ON Employee.ID = Deal.ID_Employee
WHERE Client.Name = 'Сергей'