USE CompanyDB
/*1. Провести оценку количества товара на складе по наименованиям: менее 10 – мало,
от 10 до 100 – достаточно, более 100 – избыточно.*/
GO
SELECT Product.Name, SUM(CAST(Store.Quantity AS FLOAT)) AS [Количество], CASE
    WHEN SUM(CAST(Store.Quantity AS FLOAT)) < 10 THEN 'Мало'
    WHEN SUM(CAST(Store.Quantity AS FLOAT)) BETWEEN 10 AND 100 THEN 'Достаточно'
    ELSE 'Избыточно'
    END AS [Оценка]
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product
GROUP BY Product.Name
--2. Вывести наименования товаров, количество которых на складе от 1 до 10.
GO
SELECT Product.Name, SUM(CAST(Store.Quantity AS FLOAT)) AS [Количество]
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product
GROUP BY Product.Name
HAVING SUM(CAST(Store.Quantity AS FLOAT)) BETWEEN 1 AND 10
--3. Определить тройку товаров, выручка за которые самая большая.
GO
SELECT TOP 3 Product.Name, 
    SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)) AS [Сумма]
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product
INNER JOIN Deal ON Store.ID_Product = Product.ID
GROUP BY Product.Name
ORDER BY SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)) DESC
--4. Определить суммарную стоимость продаж каждого товара по месяцам.
GO
SELECT Product.Name,
       DATEPART(YEAR, Deal.[Date]) AS [Год],
       DATEPART(MONTH, Deal.[Date]) AS [Месяц],
       SUM(Deal.Quantity * Store.Price * (1 + Deal.Discount/100)) AS [Сумма]
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product 
             INNER JOIN Deal ON Deal.ID_Store = Store.ID
GROUP BY Product.Name,
         DATEPART(YEAR, Deal.[Date]),
         DATEPART(MONTH, Deal.[Date])
/*5. Показать месяца, в которых продажи Молока 3,2% (или любого другого товара,
хранящегося на складе с разными ID) были ниже 300 денег.*/
GO
SELECT Product.Name,
       DATEPART(YEAR, Deal.[Date]) AS [Год],
       DATEPART(MONTH, Deal.[Date]) AS [Месяц],
       SUM(Deal.Quantity * Store.Price * (1 + Deal.Discount/100)) AS [Сумма]
FROM Product INNER JOIN Store ON Product.ID = Store.ID_Product 
             INNER JOIN Deal ON Deal.ID_Store = Store.ID             
GROUP BY Product.Name,
         DATEPART(YEAR, Deal.[Date]),
         DATEPART(MONTH, Deal.[Date])
HAVING SUM(Deal.Quantity * Store.Price * (1 + Deal.Discount/100)) < 30000000 
       AND Product.Name = 'NJUTNING - Vase and 6 scented sticks'        

--1) Напиши запрос, который вернет количество сделок, для каждого работника, имена работников должны быть в алфавитном порядке. 
GO
SELECT Employee.Name, COUNT(Deal.ID) AS [Количество сделок]
FROM Employee JOIN Deal ON Deal.ID_Employee = Employee.ID
GROUP BY Employee.Name
ORDER BY Employee.Name
--2) Напиши запрос, который вернет наименования тройки самых дорогих товаров       
GO 
SELECT Product.Name, Store.Price
FROM Product JOIN Store ON Product.ID = Store.ID_Product
GROUP BY Product.Name, Store.Price
ORDER BY Store.Price DESC