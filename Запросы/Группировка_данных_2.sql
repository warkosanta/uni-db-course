USE CompanyDB
--1. Ранжируйте сотрудников по объемам продаж и выведите тройку лидеров.
GO
SELECT TOP 3 Employee.Name, SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)) AS [Объем продаж], 
    RANK() OVER( ORDER BY SUM( Store.Price * Deal.Quantity * (1 + Deal.Discount/100)) DESC) AS [Место в топе]
FROM Employee INNER JOIN Deal ON Employee.ID = Deal.ID_Employee 
    INNER JOIN Store ON Deal.ID_Store = Store.ID
GROUP BY Employee.Name
/*2. Вывести суммы сделок по месяцам для каждого сотрудника и показать разницу с
предыдущим месяцем, в котором были зафиксированы сделки.*/
GO
SELECT Employee.Name, DATEPART(YEAR, Deal.[Date]) AS [Год],
       DATEPART(MONTH, Deal.[Date]) AS [Месяц],
       SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)) AS [Сумма продаж],
       -- для наглядности вывожу сумму продаж за предыдущий месяц, если она есть
       LAG (SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)), 1) 
       OVER (
           PARTITION BY Employee.Name, DATEPART(YEAR, Deal.[Date])
           ORDER BY Employee.Name) AS [Сумма продаж за предыдущий месяц],
       -- высчитываю разницу между двумя месяцами
       (SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)) -
       (LAG (SUM(Store.Price * Deal.Quantity * (1 + Deal.Discount/100)), 1) 
       OVER (
           PARTITION BY Employee.Name, DATEPART(YEAR, Deal.[Date])
           ORDER BY Employee.Name))) AS [Разница]
           
FROM Employee INNER JOIN Deal ON Employee.ID = Deal.ID_Employee
    INNER JOIN Store ON Deal.ID_Store = Store.ID
GROUP BY Employee.Name, DATEPART(YEAR, Deal.[Date]), DATEPART(MONTH, Deal.[Date]) 
