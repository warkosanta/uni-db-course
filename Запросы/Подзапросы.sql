USE CompanyDB;
GO
/*1. Вывести наименования товаров, количество которых на складе максимально (на
складе должно быть как минимум два товара, количество которых одинаково и
равно максимальному).*/
SELECT [Name] 
FROM Product
WHERE ID in 
    (
        SELECT ID_Product
        FROM Store
        WHERE Quantity = 
        (
            SELECT MAX(Quantity)
            FROM Store
        )
    )
GO
/*2. Вывести в порядке, обратном алфавитному, наименования товаров, количество
которых на складе находится в заданном диапазоне.*/
DECLARE @A FLOAT = 1
DECLARE @B FLOAT = 10

SELECT [Name]
FROM Product
WHERE ID IN 
    (
        SELECT ID_Product
        FROM Store
        WHERE Quantity BETWEEN @A AND @B
    )
ORDER BY [Name] DESC
GO
/*3. Вывести поставщиков, которые хотя бы раз осуществили поставку, в алфавитном
порядке.*/
SELECT [Name] 
FROM [Provider]
WHERE ID = ANY
    (
        SELECT ID_Provider
        FROM Store
    )
ORDER BY [Name] ASC

/*4. 30 дней с даты последней покупки действует дополнительная скидка на все товары.
Вывести список покупателей, имеющих возможность воспользоваться указанной
скидкой.*/
DECLARE @saleDays INT = 30

SELECT [Name]
FROM Client
WHERE ID IN
    (
        SELECT ID_Client
        FROM Deal
        WHERE DATEDIFF(DD, [Date], GETDATE()) <= @saleDays
    )
GO
/*5. Вывести список товаров, названия которых начинающиеся с букв «д» и «л»,
стоимость которых не более 20% от максимальной.*/
DECLARE @percent DECIMAL(4,2) = 0.2;

SELECT [Name]
FROM Product
WHERE [Name] LIKE '[N,P]%' AND ID IN
    (
        SELECT ID_Product 
        FROM Store
        WHERE Price <= 
        (
            SELECT MAX(Price) * @percent
            FROM Store
        )
    )
GO
/*6. Вывести поставщиков, которые не поставляют товары, названия которых
начинающиеся с букв «д» и «л».*/
SELECT [Name]
FROM [Provider]
WHERE ID NOT IN
    (
        SELECT ID_Provider 
        FROM Store
        WHERE ID_Product IN (
            SELECT ID
            FROM Product
            WHERE [Name] LIKE '[D,L]%'
        )
    )

GO
/*7. Показать список клиентов с указанием их пола («мужчина» или «женщина»).*/
SELECT [Name], CASE
    WHEN Sex = 'M' THEN 'Мужчина'
    WHEN Sex = 'W' THEN 'Женщина'
    END AS Gender
FROM Client
GO
-- Доп.вопрос
-- Вывести имена клиентов которые купили товар + имя товара, купленного в этом году
SELECT Client.Name AS [Cliennt name], Product.Name AS [Product name], Store.[Date] AS [Purchase date]
FROM Client INNER JOIN Deal ON Client.ID = Deal.ID_Client -- сделки клиентов
            INNER JOIN Store ON Store.ID = Deal.ID_Store -- магазин сделки
            INNER JOIN Product ON Product.ID = Store.ID_Product -- продукты магазина
WHERE DATEPART(YEAR, Store.[Date]) = DATEPART(YEAR, GETDATE())

