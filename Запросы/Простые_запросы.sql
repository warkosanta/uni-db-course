USE UniDB;
GO
-- TASK 6
SELECT COUNT(*) as NumberOfProducts
FROM dbo.[Products]
GO
-- 1 TASK
SELECT TOP 1 ProductID, Products.Title, COUNT(*) AS NumberOfProducts
FROM dbo.[Trades] JOIN Products ON Trades.ProductID = Products.ID
GROUP BY Trades.ProductID, Products.Title
ORDER BY NumberOfProducts DESC
GO
-- 2 TASK
SELECT AVG(Price) AS AverageProductPrice
FROM Products
GO
-- 3 TASK
SELECT RAND()*(-7-5)+5
GO
-- 4 TASK
SELECT COS(RADIANS(CONVERT(float, 60)))
GO
-- 5 TASK
SELECT DATENAME(MONTH,GETDATE()) AS CurrenntMonth