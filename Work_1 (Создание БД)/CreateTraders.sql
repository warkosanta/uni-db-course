USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Traders;
GO
CREATE TABLE [dbo].Traders (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Country] NVARCHAR(255) NOT NULL,
    [Сounty] NVARCHAR(255) NOT NULL,
    [City] NVARCHAR(255),
    [EMail] NVARCHAR(255) NOT NULL,
    [MobilePhone] VARCHAR(22) NOT NULL,
    CONSTRAINT PK_Traders_ID PRIMARY KEY (ID),
);
GO 
INSERT INTO [dbo].Traders([Country], [Сounty], [EMail], [MobilePhone]) VALUES
    ('Китай', 'Юньнань', 'muitzuhc@btcplay777.com','+86-135-5539-6856'),
    ('Китай', 'Цзянсу', 'unjustly@buyu495.com','+86-185-5571-1835'),
    ('Китай', 'Аньхой', 'hilfsbereitscha@animalize631xu.online','+86-135-5560-8673'),
    ('Тайвань', 'Илань', 'soepkip@cifjxgapk.shop','+86-155-5542-1203'),
    ('Парагвай', 'Итапуа', 'beadleships@giles449.top','+86-185-5551-7240');
