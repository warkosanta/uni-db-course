USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Trades;
GO
CREATE TABLE [dbo].Trades (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [ProductID] INT NOT NULL,
    [CustomerID] INT NOT NULL,
    [EmployeeID] INT NOT NULL,
    [PurchaseDatetime] DATETIME NOT NULL
    CONSTRAINT DK_Trades_PurchaseDatetime DEFAULT GETDATE(),
    CONSTRAINT PK_Trades_ID PRIMARY KEY (ID),
    CONSTRAINT FK_Trades_To_Customers FOREIGN KEY (CustomerID) REFERENCES Customers (ID) ON DELETE CASCADE,
    CONSTRAINT FK_Trades_To_Products FOREIGN KEY (ProductID) REFERENCES Products (ID) ON DELETE CASCADE,
    CONSTRAINT FK_Trades_To_Employees FOREIGN KEY (EmployeeID) REFERENCES Employees (ID) ON DELETE CASCADE,

);
GO 
INSERT INTO [dbo].Trades([ProductID], [CustomerID], [EmployeeID]) VALUES
    (1,2,3),
    (4,3,2),
    (3,3,3),
    (5,4,4),
    (3,2,5);