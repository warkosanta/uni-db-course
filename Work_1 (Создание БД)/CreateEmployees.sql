USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Employees;
GO
CREATE TABLE [dbo].Employees (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    [Surname] NVARCHAR(255) NOT NULL,
    [ShopID] INT NOT NULL,
    [IsTeaMaster] BIT NOT NULL
    CONSTRAINT DK_Employees_IsTeaMaster DEFAULT 0,
    CONSTRAINT PK_Employees_ID PRIMARY KEY (ID),
    CONSTRAINT FK_Employees_To_Shops FOREIGN KEY (ShopID) REFERENCES Shops (ID) ON DELETE CASCADE

);
GO 
INSERT INTO [dbo].Employees([Name], [Surname], [ShopID], [IsTeaMaster]) VALUES
    ('Степан', 'Абрамов',1 , 0.1),
    ('Устин', 'Кудряшов', 1, 0.15),
    ('Лука', 'Аксёнов', 2, 0.2),
    ('Болеслав', 'Фёдоров', 2, 0.25),
    ('Тимур', 'Кулагин', 4, 0.12);
