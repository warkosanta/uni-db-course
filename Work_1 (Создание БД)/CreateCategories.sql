USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Categories;
GO
CREATE TABLE [dbo].Categories (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Description] TEXT NOT NULL,
    CONSTRAINT PK_Categories_ID PRIMARY KEY (ID),
);
GO 
INSERT INTO [dbo].Categories([Description]) VALUES
    ('ЧАЙ'),
    ('ЙЕРБА МАТЕ'),
    ('ТРАВЯНОЙ СБОР'),
    ('ПОСУДА'),
    ('ТОВАРЫ ЗДОРОВОГО ПИТАНИЯ');