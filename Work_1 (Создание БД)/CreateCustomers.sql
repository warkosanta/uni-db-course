USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Customers;
GO
CREATE TABLE [dbo].Customers (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL,
    [Surname] NVARCHAR(255) NOT NULL,
    [Address] NVARCHAR(MAX),
    [EMail] NVARCHAR(255) NOT NULL,
    [MobilePhone] VARCHAR(22),
    [BonusSale] DECIMAL(5,2) NOT NULL
    CONSTRAINT DK_Customers_BonusSale DEFAULT 0.0, 
    CONSTRAINT PK_Customers_ID PRIMARY KEY (ID),
);
GO 
INSERT INTO [dbo].Customers([Name], [Surname], [EMail], [BonusSale]) VALUES
    ('Роман', 'Андрухович', 'fogumevo-4161@yopmail.com', 0.1),
    ('Феликс', 'Дидовец', 'eddocuza-1123@yopmail.com', 0.15),
    ('Фёдор ', 'Коваленко', 'soffemaqurri-0555@yopmail.com', 0.2),
    ('Олеся', 'Карпова', 'busennufo-2970@yopmail.com', 0.25),
    ('Таисия', 'Борисова', 'ojaqoze-6576@yopmail.com', 0.12);
