USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Shops;
GO
CREATE TABLE [dbo].Shops (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Country] NVARCHAR(255) NOT NULL,
    [City] NVARCHAR(255) NOT NULL,
    [Address] NVARCHAR(255) NOT NULL,
    [IsTeaClub] BIT NOT NULL
    CONSTRAINT DF_Shops_IsTeaClub DEFAULT 0,
    CONSTRAINT PK_Shops_ID PRIMARY KEY (ID),
);
GO 
INSERT INTO [dbo].Shops([Country], [City], [Address], [IsTeaClub]) VALUES
    ('Россия', 'МОСКВА', 'ул. Малая Дмитровка, 24/2 стр.2', 1),
    ('Россия', 'МОСКВА', 'ул. Мясницкая, д. 24\7, стр.2', 1),
    ('Россия', 'САНКТ-ПЕТЕРБУРГ', 'Апраксин переулок, д. 10', 1),
    ('Россия', 'ТЮМЕНЬ', 'ул. Республики 5А', 1),
    ('Россия', 'КАЛУГА', 'ул. Марата, д. 1"А"', 1);