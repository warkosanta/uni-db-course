USE UniDB;
GO
DROP TABLE IF EXISTS [dbo].Products;
GO
CREATE TABLE [dbo].Products (
    [ID] INT IDENTITY(1,1) NOT NULL,
    [CategoryID] INT NOT NULL,
    [TraderID] INT NOT NULL,
    [Title] NVARCHAR(255) NOT NULL,
    [Description] TEXT,
    [Price] MONEY NOT NULL,
    CONSTRAINT PK_Products_ID PRIMARY KEY (ID),
    CONSTRAINT FK_Products_To_Categories FOREIGN KEY (CategoryID) REFERENCES Categories (ID) ON DELETE CASCADE,
    CONSTRAINT FK_Products_To_Traders FOREIGN KEY (TraderID) REFERENCES Traders (ID) ON DELETE CASCADE,

);
GO 
INSERT INTO [dbo].Products([CategoryID], [TraderID], [Title], [Price]) VALUES
    (1, 1, 'Гуогань Шу Ча', 697.0),
    (1, 2,'Цзин Чжи Улун Ча', 2300.0),
    (2, 3,'Парагвайский традиционный мате Мойчай', 780.0),
    (1, 3,'Цзинь Ло', 1160.0),
    (4, 4,'Чайник из исинской глины', 9460.0);